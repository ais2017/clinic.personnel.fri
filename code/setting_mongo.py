import pymongo

conn = pymongo.MongoClient()

#ATTENTION REMOVE!!!
conn.drop_database('clinic')

db = conn['clinic']
coll = db['employee']
db.employee.create_index([('login', pymongo.ASCENDING)], unique=True)

coll.save({"login": "admin", "typeuser": 3, })
coll.save({"login": "reg", "typeuser": 2, })
