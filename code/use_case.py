from classes import *

class Stub(object):
    def __init__(self):
        self.employers = []

    def foundSameEmployee(self, empl):
        emplList = []
        info_search = empl.getPersonalInfo()
        fio_search = info_search.getFIO()
        list_search = [fio_search.getName(), fio_search.getSurname(), fio_search.getMidName(),
                       info_search.getCompetence(), info_search.getPosition(), info_search.getRank(), info_search.getDateEnd()]
        for e in self.employers:
            if e.getLogin() is not None and e.getLogin() == empl.getLogin():
                emplList.append(e)
                continue
            info = e.getPersonalInfo()
            if info is not None:
                if info.getNumD() is not None and info.getNumD() == info_search.getNumD():
                    emplList.append(e)
                    continue
                fio = info.getFIO()
            else:
                fio = FullName(None, None, None)
                info = PersonalInfo(None, None, None, None, None, None)
            list = [fio.getName(), fio.getSurname(), fio.getMidName(),
                        info.getCompetence(), info.getPosition(), info.getRank(), info.getDateEnd()]
            tmp = True
            for cur, search in zip(list, list_search):
                if search is None or cur == search:
                    continue
                else:
                    tmp = False
                    break
            if tmp:
                emplList.append(e)
        return emplList

    def save(self, empl):
        for e in self.employers:
            if e.getLogin() == empl.getLogin():
                self.employers.remove(e)
        self.employers.append(empl)

    def chooseEmployee(self, numEmpl):
        return numEmpl

    def changeData(self, old, new):
        self.employers.remove(old)
        self.employers.append(new)

    def getOwnData(self, login):
        for e in self.employers:
            if e.getLogin() == login:
                return e

def get_type_user(stub, login):
    data = stub.getOwnData(login)
    if data == None:
        return 0
    return data.usertype

def view_your_data(stub, login):
    data = stub.getOwnData(login)
    return data

def employee_data_request(stub, empl):
    search_empl = empl
    emplSameList = stub.foundSameEmployee(search_empl)
    if len(emplSameList) == 0:
        return []
    return emplSameList

def change_password(stub, empl_login, old_pass, new_pass):
    data = stub.getOwnData(empl_login)
    if not data.check_password(old_pass):
        return "Invalid password!"
    new_empl_data = Employee(data.getLogin(), new_pass, data.getPersonalInfo())
    stub.changeData(data, new_empl_data)
    return "Password changed!"

def employee_registration(stub, stub_new_empl):
    new_empl = stub_new_empl
    emplSameList = stub.foundSameEmployee(new_empl)
    if len(emplSameList) != 0:
        for e in emplSameList:
            if e.getLogin() == new_empl.getLogin():
                return "User with same login already exist!"
            if e.getNumD() == new_empl.getNumD():
                raise "User with same nubmer of diplom already exist!"

    stub.save(new_empl)
    return "New user added!"

def change_employee_data(stub, stub_old_empl, stub_new_empl, numChooseEmpl):
    change_empl = stub_old_empl
    emplSameList = stub.foundSameEmployee(change_empl)

    if len(emplSameList) == 0:
        raise Exception("This employee not found!")

    choose = stub.chooseEmployee(numChooseEmpl)
    old_info_epl = emplSameList[choose]

    new_info_empl = stub_new_empl
    emplSameList = stub.foundSameEmployee(new_info_empl)
    if len(emplSameList) != 0:
        for e in emplSameList:
            if e != old_info_epl:
                if e.getLogin() == new_info_empl.getLogin():
                    raise Exception("User with same login already exist!")
                if e.getNumD() == new_info_empl.getNumD():
                    raise Exception("User with same nubmer of diplom already exist!")
    stub.changeData(old_info_epl, new_info_empl)
    return "Info changed!"
