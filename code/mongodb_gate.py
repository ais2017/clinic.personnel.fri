import pymongo
from classes import *


class gate(object):
    def __init__(self):
        self.conn = pymongo.MongoClient()
        self.db = self.conn['clinic']
        self.coll = self.db['employee']
        #self.db.employee.create_index([('login', pymongo.ASCENDING)], unique=True)

        #self.coll.save({"login": "admin", "typeuser": 3,})
        #self.coll.save({"login": "reg", "typeuser": 2, })

    def foundSameEmployee(self, empl):
        emplList = []
        info_search = empl.getPersonalInfo()
        fio_search = info_search.getFIO()
        list_search = [fio_search.getName(), fio_search.getSurname(), fio_search.getMidName(),
                       info_search.getCompetence(), info_search.getPosition(), info_search.getRank(), info_search.getDateEnd(), empl.getLogin(), info_search.getNumD()]
        list_dict = ['name', 'surname', 'midname', 'competence', 'position', 'rank', 'edata', 'login',  'numd']

        request = dict()
        for i in range(len(list_search)):
            if list_search[i] is not None:
                request[list_dict[i]] = list_search[i]

        emplList = []
        for men in self.coll.find(request):
            login = men.get("login")
            Name = men.get("name")
            Surname = men.get("surname")
            MidName = men.get("midname")
            Competence = men.get("competence")
            Position = men.get("position")
            rank = men.get("rank")
            NumD = men.get("numd")
            edata = men.get("edata")
            fio = FullName(Surname, Name, MidName)
            info = PersonalInfo(fio, Position, Competence, rank, NumD, edata)
            empl = Employee(login, None, info)
            emplList.append(empl)
        return emplList

    def save(self, empl):
        info_new = empl.getPersonalInfo()
        fio_new = info_new.getFIO()
        list_search = [fio_new.getName(), fio_new.getSurname(), fio_new.getMidName(),
                       info_new.getCompetence(), info_new.getPosition(), info_new.getRank(),
                       info_new.getDateEnd(), empl.getLogin(), info_new.getNumD()]
        list_dict = ['name', 'surname', 'midname', 'competence', 'position', 'rank', 'edata', 'login', 'numd']
        doc = dict()
        for i in range(len(list_search)):
            if list_search[i] is not None:
                doc[list_dict[i]] = list_search[i]

        doc["typeuser"] = 1
        self.coll.save(doc)

    def changeData(self, old, new):
        self.employers.remove(old)
        self.employers.append(new)

    def getOwnData(self, login):
        for men in self.coll.find({"login": login}):
            if int(men["typeuser"]) == 1:
                login = men.get("login")
                Name = men.get("name")
                Surname = men.get("surname")
                MidName = men.get("midname")
                Competence = men.get("competence")
                Position = men.get("position")
                rank = men.get("rank")
                NumD = men.get("numd")
                edata = men.get("edata")
                fio = FullName(Surname, Name, MidName)
                info = PersonalInfo(fio, Position, Competence, rank, NumD, edata)
                empl = Employee(login, None, info)
            else:
                return Employee(login, None, None, usertype=int(men["typeuser"]))
            return  empl
