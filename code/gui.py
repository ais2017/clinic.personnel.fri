# -*- coding: utf-8 -*-
from use_case import *
from classes import *

import sys
from PyQt5 import QtCore, QtGui, QtWidgets

import loggin_gui
import Employee_gui
import Registrator_gui
import view_your_data_gui
import change_password_gui
import employee_data_request_gui
import register_employee_gui
import change_data_gui

import mongodb_gate


#############################
#stub = Stub()
#
#dataEnd = datetime.date(2012, 10, 13)
#fio = FullName("Ivanov", "Ivan", "Ivanich")
#info = PersonalInfo(fio, "Intern", "dantist", "PhD", 123, dataEnd)
#empl_1 = Employee("login", "password", info)
#
#dataEnd = datetime.date(2012, 9, 23)
#fio = FullName("Petrov", "Ivan", "Ivanich")
#info = PersonalInfo(fio, "Intern2", "dantist2", "PhD", 1234, dataEnd)
#empl_2 = Employee("login2", "password", info)
#
#emplAdmin = Employee("admin", "pa", usertype=3)
#emplRegister = Employee("reg", "ra", usertype=2)
#stub.save(empl_2)
#stub.save(empl_1)
#stub.save(emplAdmin)
#stub.save(emplRegister)
#################################
#MongoDB
stub = mongodb_gate.gate()
####################


class Model(QtCore.QAbstractTableModel):
    def __init__(self, parent):
        QtCore.QAbstractTableModel.__init__(self)
        self.gui = parent
        self.colLabels = ['Login', 'Name', 'Surname', 'MidName', 'Competence', 'Position', 'rank', 'NumD', 'expiration date']
        self.cached = None
    def rowCount(self, parent):
        return len(self.cached)
    def columnCount(self, parent):
        return len(self.colLabels)
    def data(self, index, role):
        if not index.isValid():
            return QtCore.QVariant()
        elif role != QtCore.Qt.DisplayRole and role != QtCore.Qt.EditRole:
            return QtCore.QVariant()
        value = ''
        if role == QtCore.Qt.DisplayRole:
            row = index.row()
            col = index.column()
            value = self.cached[row][col]
        return QtCore.QVariant(value)
    def headerData(self, section, orientation, role):
        if orientation == QtCore.Qt.Horizontal and role == QtCore.Qt.DisplayRole:
            return QtCore.QVariant(self.colLabels[section])
        return QtCore.QVariant()

class LogWin(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = loggin_gui.Ui_MainWindow()
        self.ui.setupUi(self)
        self.EmplWin = None
        self.AdminWin = None
        self.RegistWin = None

        self.ui.pushButton.clicked.connect(self.SignIn)


    def SignIn(self):
        login = self.ui.lineEdit.text()
        password = self.ui.lineEdit_2.text()
        #STUB
        type_user = get_type_user(stub, login)
        #send login and password to db and check
        # type_user 0 - error; 1 - empl; 2 - regist; 3 - admin
        #type_user = 1
        if type_user == 0:
            QtWidgets.QMessageBox.warning(self, 'Error', 'Invalid user or password!')
        if type_user == 1:
            if not self.EmplWin:
                self.EmplWin = EmplWin(self, login, password)
            self.EmplWin.show()
            self.hide()
        if type_user == 2:
            if not self.RegistWin:
                self.RegistWin = RegWin(self)
            self.RegistWin.show()
            self.hide()
        if type_user == 3:
            if not self.AdminWin:
                self.AdminWin = EmpoyeeDataRequest(self)
            self.AdminWin.show()
            self.hide()

class EmplWin(QtWidgets.QMainWindow):
    def __init__(self, parent=None, login=None, password=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Employee_gui.Ui_MainWindow()
        self.ui.setupUi(self)
        self.dataWin = None
        self.PassWin = None
        self.login = login
        self.password = password

        self.ui.pushButton.clicked.connect(self.view_data)
        self.ui.pushButton_2.clicked.connect(self.change_pass)

    def view_data(self):
        if not self.dataWin:
            self.dataWin = View_data(self, self.login)
        self.dataWin.show()

    def change_pass(self):
        if not self.PassWin:
            self.PassWin = ChangePassword(self, self.login)
        self.PassWin.show()


class RegWin(QtWidgets.QMainWindow):
    def __init__(self, parent=None, login=None, password=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Registrator_gui.Ui_MainWindow()
        self.ui.setupUi(self)
        self.regWin = None
        self.changeWin = None

        self.ui.pushButton.clicked.connect(self.registration)
        self.ui.pushButton_2.clicked.connect(self.change)

    def registration(self):
        if not self.regWin:
            self.regWin = registaration_employee(self)
        self.regWin.show()

    def change(self):
        if not self.changeWin:
            self.changeWin = ChangeData(self)
        self.changeWin.show()

class View_data(QtWidgets.QMainWindow):
    def __init__(self, parent=None, login=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = view_your_data_gui.Ui_MainWindow()
        self.ui.setupUi(self)

        #Stub get
        empl = view_your_data(stub, login)
        info = empl.getPersonalInfo()
        fio = info.getFIO()
        endDate = info.getDateEnd()
        if endDate is not None:
            strDate = endDate.strftime('%Y-%m-%d')
        else:
            strDate = None

        #TODO show vacation and Graph

        model = Model(self.ui.tableView)
        model.cached = [[login, fio.getName(), fio.getSurname(), fio.getMidName(), info.getCompetence(), info.getPosition(), info.getRank(), info.getNumD(), strDate]]
        self.ui.tableView.setModel(model)

class ChangePassword(QtWidgets.QMainWindow):
    def __init__(self, parent=None, login=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = change_password_gui.Ui_MainWindow()
        self.ui.setupUi(self)
        self.login = login

        self.ui.pushButton.clicked.connect(self.change)

    def change(self):
        oldPas = self.ui.lineEdit.text()
        newPas = self.ui.lineEdit_2.text()
        newPas2 = self.ui.lineEdit_3.text()
        if newPas != newPas2:
            QtWidgets.QMessageBox.warning(self, 'Error', 'Entered passwords do not match!')
        else:
            if change_password(stub, self.login, oldPas, newPas) == "Invalid password!":
                QtWidgets.QMessageBox.warning(self, 'Error', 'Invalid password!')
            else:
                QtWidgets.QMessageBox.warning(self, 'Error', 'Password was changed!')
        self.close()

class EmpoyeeDataRequest(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = employee_data_request_gui.Ui_MainWindow()
        self.ui.setupUi(self)

        self.ui.tableWidget.setColumnCount(9)
        self.ui.tableWidget.setRowCount(1)
        self.ui.tableWidget.setHorizontalHeaderLabels(['Login', 'Name', 'Surname', 'MidName', 'Competence', 'Position', 'rank', 'NumD', 'expiration date'])
        self.ui.pushButton.clicked.connect(self.found)

    def found(self):
        login = None
        Name = None
        Surname = None
        MidName = None
        Competence = None
        Position = None
        rank = None
        NumD = None
        edata = None
        if self.ui.tableWidget.item(0, 0) and self.ui.tableWidget.item(0, 0).text() != '':
            login = self.ui.tableWidget.item(0, 0).text()
        if self.ui.tableWidget.item(0, 1) and self.ui.tableWidget.item(0, 1).text() != '':
            Name = self.ui.tableWidget.item(0, 1).text()
        if self.ui.tableWidget.item(0, 2) and self.ui.tableWidget.item(0, 2).text() != '':
            Surname = self.ui.tableWidget.item(0, 2).text()
        if self.ui.tableWidget.item(0, 3) and self.ui.tableWidget.item(0, 3).text() != '':
            MidName = self.ui.tableWidget.item(0, 3).text()
        if self.ui.tableWidget.item(0, 4) and self.ui.tableWidget.item(0, 4).text() != '':
            Competence = self.ui.tableWidget.item(0, 4).text()
        if self.ui.tableWidget.item(0, 5) and self.ui.tableWidget.item(0, 5).text() != '':
            Position = self.ui.tableWidget.item(0, 5).text()
        if self.ui.tableWidget.item(0, 6) and self.ui.tableWidget.item(0, 6).text() != '':
            rank = self.ui.tableWidget.item(0, 6).text()
        if self.ui.tableWidget.item(0, 7) and self.ui.tableWidget.item(0, 7).text() != '':
            NumD = self.ui.tableWidget.item(0, 7).text()
        if self.ui.tableWidget.item(0, 8) and self.ui.tableWidget.item(0, 8).text() != '':
            edata = self.ui.tableWidget.item(0, 8).text()

        fio = FullName(Surname, Name, MidName)
        info = PersonalInfo(fio, Position, Competence, rank, NumD, edata)
        empl = Employee(login, None, info)
        list_empl = employee_data_request(stub, empl)

        model = Model(self.ui.tableView)
        model.cached = []
        for e in list_empl:
            info = e.getPersonalInfo()
            fio = info.getFIO()
            endDate = info.getDateEnd()
            if endDate is not None:
                strDate = endDate.strftime('%Y-%m-%d')
            else:
                strDate = None
            line = [e.getLogin(), fio.getName(), fio.getSurname(), fio.getMidName(), info.getCompetence(), info.getPosition(), info.getRank(), info.getNumD(), strDate]
            model.cached.append(line)
        self.ui.tableView.setModel(model)

class ChangeData(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = change_data_gui.Ui_MainWindow()
        self.ui.setupUi(self)

        self.ui.tableWidget.setColumnCount(9)
        self.ui.tableWidget.setRowCount(1)
        self.ui.tableWidget.setHorizontalHeaderLabels(['Login', 'Name', 'Surname', 'MidName', 'Competence', 'Position', 'rank', 'NumD', 'expiration date'])

        self.ui.pushButton.clicked.connect(self.found)
        self.ui.pushButton.clicked.connect(self.change)

    def found(self):
        login = None
        Name = None
        Surname = None
        MidName = None
        Competence = None
        Position = None
        rank = None
        NumD = None
        edata = None
        if self.ui.tableWidget.item(0, 0) and self.ui.tableWidget.item(0, 0).text() != '':
            login = self.ui.tableWidget.item(0, 0).text()
        if self.ui.tableWidget.item(0, 1) and self.ui.tableWidget.item(0, 1).text() != '':
            Name = self.ui.tableWidget.item(0, 1).text()
        if self.ui.tableWidget.item(0, 2) and self.ui.tableWidget.item(0, 2).text() != '':
            Surname = self.ui.tableWidget.item(0, 2).text()
        if self.ui.tableWidget.item(0, 3) and self.ui.tableWidget.item(0, 3).text() != '':
            MidName = self.ui.tableWidget.item(0, 3).text()
        if self.ui.tableWidget.item(0, 4) and self.ui.tableWidget.item(0, 4).text() != '':
            Competence = self.ui.tableWidget.item(0, 4).text()
        if self.ui.tableWidget.item(0, 5) and self.ui.tableWidget.item(0, 5).text() != '':
            Position = self.ui.tableWidget.item(0, 5).text()
        if self.ui.tableWidget.item(0, 6) and self.ui.tableWidget.item(0, 6).text() != '':
            rank = self.ui.tableWidget.item(0, 6).text()
        if self.ui.tableWidget.item(0, 7) and self.ui.tableWidget.item(0, 7).text() != '':
            NumD = self.ui.tableWidget.item(0, 7).text()
        if self.ui.tableWidget.item(0, 8) and self.ui.tableWidget.item(0, 8).text() != '':
            edata = self.ui.tableWidget.item(0, 8).text()

        fio = FullName(Surname, Name, MidName)
        info = PersonalInfo(fio, Position, Competence, rank, NumD, edata)
        empl = Employee(login, None, info)
        list_empl = employee_data_request(stub, empl)

        self.ui.tableWidget_2.setColumnCount(9)
        self.ui.tableWidget_2.setRowCount(len(list_empl))
        self.ui.tableWidget_2.setHorizontalHeaderLabels(['Login', 'Name', 'Surname', 'MidName', 'Competence', 'Position', 'rank', 'NumD', 'expiration date'])
        count_empl = 0;
        for e in list_empl:
            info = e.getPersonalInfo()
            fio = info.getFIO()
            endDate = info.getDateEnd()
            if endDate is not None:
                strDate = endDate.strftime('%Y-%m-%d')
            else:
                strDate = None
            line = [e.getLogin(), fio.getName(), fio.getSurname(), fio.getMidName(), info.getCompetence(),
                    info.getPosition(), info.getRank(), info.getNumD(), strDate]
            for i in range(len(line)):
                if line[i] is not None:
                    self.ui.tableWidget_2.setItem(count_empl, i, QtWidgets.QTableWidgetItem(str(line[i])))
                else:
                    self.ui.tableWidget_2.setItem(count_empl, i, QtWidgets.QTableWidgetItem(""))
            count_empl += 1

    def change(self):
        pass



class registaration_employee(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = register_employee_gui.Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.tableWidget.setColumnCount(9)
        self.ui.tableWidget.setRowCount(1)
        self.ui.tableWidget.setHorizontalHeaderLabels(['Login', 'Name', 'Surname', 'MidName', 'Competence', 'Position', 'rank', 'NumD', 'expiration date'])

        self.ui.pushButton.clicked.connect(self.registration)

    def registration(self):
        login = None
        Name = None
        Surname = None
        MidName = None
        Competence = None
        Position = None
        rank = None
        NumD = None
        edata = None
        if self.ui.tableWidget.item(0, 0) and self.ui.tableWidget.item(0, 0).text() != '':
            login = self.ui.tableWidget.item(0, 0).text()
        if self.ui.tableWidget.item(0, 1) and self.ui.tableWidget.item(0, 1).text() != '':
            Name = self.ui.tableWidget.item(0, 1).text()
        if self.ui.tableWidget.item(0, 2) and self.ui.tableWidget.item(0, 2).text() != '':
            Surname = self.ui.tableWidget.item(0, 2).text()
        if self.ui.tableWidget.item(0, 3) and self.ui.tableWidget.item(0, 3).text() != '':
            MidName = self.ui.tableWidget.item(0, 3).text()
        if self.ui.tableWidget.item(0, 4) and self.ui.tableWidget.item(0, 4).text() != '':
            Competence = self.ui.tableWidget.item(0, 4).text()
        if self.ui.tableWidget.item(0, 5) and self.ui.tableWidget.item(0, 5).text() != '':
            Position = self.ui.tableWidget.item(0, 5).text()
        if self.ui.tableWidget.item(0, 6) and self.ui.tableWidget.item(0, 6).text() != '':
            rank = self.ui.tableWidget.item(0, 6).text()
        if self.ui.tableWidget.item(0, 7) and self.ui.tableWidget.item(0, 7).text() != '':
            NumD = self.ui.tableWidget.item(0, 7).text()
        if self.ui.tableWidget.item(0, 8) and self.ui.tableWidget.item(0, 8).text() != '':
            edata = self.ui.tableWidget.item(0, 8).text()

        if login is None or NumD is None:
            QtWidgets.QMessageBox.warning(self, 'Error', 'Login and NumD is required!')
        else:
            fio = FullName(Surname, Name, MidName)
            info = PersonalInfo(fio, Position, Competence, rank, NumD, edata)
            empl = Employee(login, None, info)
            msg = employee_registration(stub, empl)
            QtWidgets.QMessageBox.warning(self, 'Message', msg)


if __name__=="__main__":
    app = QtWidgets.QApplication(sys.argv)
    myapp = LogWin()
    myapp.show()
    sys.exit(app.exec_())