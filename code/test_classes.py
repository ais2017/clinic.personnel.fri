import unittest
from classes import *


class tests(unittest.TestCase):

    def setUp(self):
        self.surname = "Ivanov"
        self.name = "Ivan"
        self.midname = "Ivanich"
        self.year = 2010
        self.month = 5
        self.days = [4, 5]
        self.TodayDate = datetime.date.today()
        self.MaxDate = datetime.date.max
        self.ts = [datetime.time.min]
        self.te = [datetime.time.max]
        self.pos = "Intern"
        self.competence = "dantist"
        self.rank = "Phd"
        self.numD = 123
        self.dataEnd = datetime.date(2012, 10, 13)

    def test_FIO_init(self):
        fio = FullName(self.surname, self.name, self.midname)
        self.assertEqual(fio.getName(), self.name)
        self.assertEqual(fio.getSurname(), self.surname)
        self.assertEqual(fio.getMidName(), self.midname)

    def test_graph_init(self):
        g = Graph(self.year, self.month, self.days, self.ts, self.te)
        self.assertEqual(g.getYear(), self.year)
        self.assertEqual(g.getMonth(), self.month)
        self.assertEqual(g.getDays(), self.days)
        self.assertEqual(g.getTE(), self.te)
        self.assertEqual(g.getTS(), self.ts)
        with self.assertRaises(Exception):
            Graph(self.year, self.month, self.days, self.te, self.ts)

    def test_vacation_init(self):
        v = Vacation(self.TodayDate, self.MaxDate)
        self.assertEqual(v.getStartDate(), self.TodayDate)
        self.assertEqual(v.getEndDate(), self.MaxDate)
        with self.assertRaises(Exception):
            Vacation(self.MaxDate, self.TodayDate)

    def test_personalInfo_init(self):
        fio = FullName(self.surname, self.name, self.midname)
        info = PersonalInfo(FIO=fio, position=self.pos, competence=self.competence, rank=self.rank, numD=self.numD, dataEnd=self.dataEnd)
        self.assertEqual(info.getFIO(), fio)
        self.assertEqual(info.getCompetence(), self.competence)
        self.assertEqual(info.getPosition(), self.pos)
        self.assertEqual(info.getRank(), self.rank)
        self.assertEqual(info.getNumD(), self.numD)
        self.assertEqual(info.getDateEnd(), self.dataEnd)

    def test_vacation_and_workgraph(self):
        fio = FullName(self.surname, self.name, self.midname)
        info = PersonalInfo(fio, self.pos, self.competence, self.rank, self.numD, self.dataEnd)

        g = Graph(self.year, self.month, self.days, self.ts, self.te)
        g2 = Graph(self.year + 1, self.month, self.days, self.ts, self.te)
        info.addWorkGraph(g)
        self.assertEqual(info.getWorkGraph(), [g])
        info.addWorkGraph(g2)
        info.addWorkGraph(g2)
        self.assertEqual(info.getWorkGraph(), [g, g2])
        info.delWorkGraph(self.year, self.month + 1)
        self.assertEqual(info.getWorkGraph(), [g, g2])
        info.delWorkGraph(self.year, self.month)
        self.assertEqual(info.getWorkGraph(), [g2])

        v = Vacation(self.TodayDate, self.MaxDate)
        MinDate = datetime.date.min
        v2 = Vacation(MinDate, self.MaxDate)
        info.addVacation(v)
        info.addVacation(v2)
        info.addVacation(v2)
        self.assertEqual(info.getVacataion(), [v, v2])
        info.delVacation(self.TodayDate)
        self.assertEqual(info.getVacataion(), [v2])

    def test_employee(self):
        login = "login"
        password = "password"
        fio = FullName(self.surname, self.name, self.midname)
        info = PersonalInfo(fio, self.pos, self.competence, self.rank, self.numD, self.dataEnd)
        empl = Employee(login, password, info)
        self.assertEqual(empl.getLogin(), login)
        self.assertEqual(empl.getPersonalInfo(), info)
        self.assertTrue(empl.check_password(password))
        self.assertFalse(empl.check_password("false_password"))


if __name__ == '__main__':
    unittest.main()
