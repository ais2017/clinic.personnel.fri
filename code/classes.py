import datetime
from werkzeug.security import check_password_hash, generate_password_hash


class FullName(object):
    def __init__(self, F, I, O):
        self.F = F
        self.I = I
        self.O = O

    def getName(self):
        return self.I

    def getSurname(self):
        return  self.F

    def getMidName(self):
        return self.O

class Graph(object):
    def __init__(self, year, month, days, times_start, times_end):
        if days.__len__() > 6:
            raise Exception("Invalid amount days schedule!")
        for d in days:
            if d > 6 or d < 0:
                raise Exception("Invalid days schedule!")
        if len(times_start) != len(times_end):
            raise Exception("Invalid time schedule!")
        for s, e in zip(times_start, times_end):
            ss = s.hour * 3600 + s.minute * 60 + s.second
            se = e.hour * 3600 + e.minute * 60 + e.second
            if se - ss <= 0:
                raise Exception("Invalid time scheduler!")
        self.year = year
        self.month = month
        self.days = days
        self.times_start = times_start
        self.times_end = times_end

    def getYear(self):
        return self.year

    def getMonth(self):
        return self.month

    def getDays(self):
        return self.days

    def getTS(self):
        return self.times_start

    def getTE(self):
        return self.times_end

class Vacation(object):
    def __init__(self, startDay, endDay):
        dif = datetime.date.toordinal(endDay) - datetime.date.toordinal(startDay)
        if dif < 0:
            raise Exception("Invalid Vacation date!")
        self.startDate = startDay
        self.endDate = endDay

    def getStartDate(self):
        return self.startDate

    def getEndDate(self):
        return self.endDate

    def setStartDate(self, date):
        self.startDate = date

    def setEndDate(self, date):
        self.endDate = date

class PersonalInfo(object):
    def __init__(self, FIO, position, competence, rank, numD, dataEnd):
        self.FIO = FIO
        self.position = position
        self.competence = competence
        self.rank = rank
        self.numD = numD
        self.dataEnd = dataEnd
        self.workGraph = []
        self.Vacation = []

    def getFIO(self):
        return self.FIO

    def getPosition(self):
        return self.position

    def getCompetence(self):
        return self.competence

    def getRank(self):
        return self.rank

    def getNumD(self):
        return self.numD

    def getDateEnd(self):
        return self.dataEnd

    def addWorkGraph(self, graph):
        for cur_g in self.workGraph:
            if cur_g == graph:
                self.workGraph.remove(cur_g)
                break
        self.workGraph.append(graph)

    def addVacation(self, vacation):
        for cur_v in self.Vacation:
            if cur_v == vacation:
                self.Vacation.remove(cur_v)
                break
        self.Vacation.append(vacation)

    def delWorkGraph(self, year, month):
        for wg in self.workGraph:
            if wg.getYear() == year and wg.getMonth() == month:
                self.workGraph.remove(wg)
                break

    def delVacation(self, sDate):
        for v in self.Vacation:
            if v.getStartDate() == sDate:
                self.Vacation.remove(v)
                break

    def getVacataion(self):
        return self.Vacation

    def getWorkGraph(self):
        return self.workGraph

class Employee(object):
    def __init__(self, login, password, Info = None, usertype = 1):
        self.login = login
        self.password_hash = None
        if password is not None:
            self.password_hash = generate_password_hash(password)
        self.Info = Info
        self.usertype = usertype

    def getPersonalInfo(self):
        return self.Info

    def getLogin(self):
        return self.login

    def setPersonalInfo(self, info):
        self.info = info

    def setLogin(self, login):
        self.login = login

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)
