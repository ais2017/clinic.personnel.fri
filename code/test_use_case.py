import unittest
from use_case import *

class tests_use_case(unittest.TestCase):
    def setUp(self):
        dataEnd = datetime.date(2012, 10, 13)
        fio = FullName("Ivanov", "Ivan", "Ivanich")
        info = PersonalInfo(fio, "Intern", "dantist", "PhD", 123, dataEnd)
        self.empl_1 = Employee("login", "password", info)
        dataEnd = datetime.date(2014, 10, 13)
        fio = FullName("Petrov", "Petr", "Petrovich")
        info = PersonalInfo(fio, "vrach", "terapevt", "Doc", 12345, dataEnd)
        self.empl_2 = Employee("login2", "password", info)
        dataEnd = datetime.date(2015, 10, 13)
        fio = FullName("Sergeev", "Sergey", "Sergeevich")
        info = PersonalInfo(fio, "sanitar", "lor", "bac", 543, dataEnd)
        self.empl_3 = Employee("login", "password", info)
        dataEnd = datetime.date(1999, 10, 13)
        fio = FullName("Vasilev", "Vasili", "Ivanovich")
        info = PersonalInfo(fio, "vrach", "lor", "PhD", 555, dataEnd)
        self.empl_4 = Employee("login4", "password", info)
        dataEnd = datetime.date(1999, 10, 11)
        fio = FullName("Aleksanrov", "Alekasandr", "Aleksandrovich")
        info = PersonalInfo(fio, "vrach", "uzi", "bac", 1555, dataEnd)
        self.empl_5 = Employee("login5", "password", info)
        dataEnd = datetime.date(1999, 10, 11)
        fio = FullName(None, None, None)
        info = PersonalInfo(fio, "vrach", None, None, None, None)
        self.empl_tmp = Employee(None, "password", info) #empl_4

    def test_view_your_data(self):
        stub = Stub()
        stub.save(self.empl_1)
        stub.save(self.empl_2)
        self.assertEqual(view_your_data(stub, self.empl_2.getLogin()), self.empl_2)
        self.assertEqual(view_your_data(stub, self.empl_1.getLogin()), self.empl_1)

    def test_employee_data_request(self):
        stub = Stub()
        stub.save(self.empl_1)
        stub.save(self.empl_2)
        stub.save(self.empl_4)
        self.assertEqual(employee_data_request(stub, self.empl_tmp), [self.empl_2, self.empl_4])

    def test_change_password(self):
        stub = Stub()
        stub.save(self.empl_1)
        stub.save(self.empl_2)
        self.assertEqual(change_password(stub, self.empl_1.getLogin(), "false", "loggin"), "Invalid password!")
        self.assertEqual(change_password(stub, self.empl_1.getLogin(), "password", "another_pas"), "Password changed!")
        new_data = stub.getOwnData(self.empl_1.getLogin())
        self.assertTrue(new_data.check_password("another_pas"))

    def test_employee_registration(self):
        stub = Stub()
        stub.save(self.empl_1)
        stub.save(self.empl_2)
        self.assertEqual(employee_registration(stub, self.empl_3), "User with same login already exist!")
        self.assertEqual(employee_registration(stub, self.empl_4), "New user added!")
        self.assertEqual(stub.employers, [self.empl_1, self.empl_2, self.empl_4])

    def test_change_employee_data(self):
        stub = Stub()
        stub.save(self.empl_1)
        stub.save(self.empl_2)
        with self.assertRaises(Exception):
            change_employee_data(stub, self.empl_5, self.empl_4, 0)
        self.assertEqual(employee_registration(stub, self.empl_4), "New user added!")
        self.assertEqual(change_employee_data(stub, self.empl_tmp, self.empl_5, 0), "Info changed!")
        self.assertEqual(stub.employers, [self.empl_1, self.empl_4, self.empl_5])


if __name__ == '__main__':
    unittest.main()
